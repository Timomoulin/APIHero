insert into weapon (name,attack_bonus) values ('hache',2);
insert into weapon (name,attack_bonus) values ('master-sword',4);

insert into hero (name,hp,attack,defense) values ('Gimli',200,2,4);
insert into hero (name,hp,attack,defense) values ('Zelda',100,5,3);
insert into hero (name,hp,attack,defense) values ('Aragorn',200,3,4);
insert into hero (name,hp,attack,defense) values ('Legolas',200,2,4);