package com.greta.demoherosapi.services;

import com.greta.demoherosapi.models.Hero;
import com.greta.demoherosapi.models.Weapon;
import com.greta.demoherosapi.repositories.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class WeaponService  {

    private WeaponRepository weaponRepository;

    @Autowired
    public WeaponService(WeaponRepository weaponRepository) {
        this.weaponRepository = weaponRepository;
    }

    public Collection<Weapon> getAll(){
        return weaponRepository.findAll();
    }

    public Weapon getOne(Long id){
        return weaponRepository.findById(id).get();
    }


}
