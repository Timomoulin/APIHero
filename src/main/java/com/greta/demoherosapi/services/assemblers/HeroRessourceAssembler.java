package com.greta.demoherosapi.services.assemblers;

import com.greta.demoherosapi.controllers.HeroController;
import com.greta.demoherosapi.controllers.WeaponController;
import com.greta.demoherosapi.models.Hero;
import com.greta.demoherosapi.models.Weapon;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class HeroRessourceAssembler implements RepresentationModelAssembler<Hero, EntityModel<Hero>>, RepresentationModelProcessor<EntityModel<Hero>> {

    @Override
    public EntityModel<Hero> toModel(Hero hero) {


        EntityModel<Hero> hero2= EntityModel.of(hero,//
               linkTo(methodOn(HeroController.class).one(hero.getIdHero())).withSelfRel(),
               linkTo(methodOn(HeroController.class).all()).withRel("heroes"));
               //linkTo(methodOn(WeaponController.class).one(hero.getWeapon().getIdWeapon())).withRel("weapon"));
       // hero2.add(linkTo(methodOn(WeaponController.class).one(hero.getWeapon().getIdWeapon())).withRel("weapon").withRel("weapon").expand(hero2.getContent().getWeapon()));

       return hero2;
    }
    public List<EntityModel<Hero>> toModel(List <Hero> heroes){
        return heroes.stream()//
                .map(this::toModel)//
                .collect(Collectors.toList());
    }

    @Override
    public CollectionModel<EntityModel<Hero>> toCollectionModel(Iterable<? extends Hero> heroes) {

        return RepresentationModelAssembler.super.toCollectionModel(heroes);
    }

    @Override
    public EntityModel<Hero> process(EntityModel<Hero> model) {

       // model.add(linkTo(methodOn(WeaponController.class).one(model.getContent().getWeapon().getIdWeapon())).withRel("weapon"));

        return model;
    }
}
