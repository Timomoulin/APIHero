package com.greta.demoherosapi.services.assemblers;

import com.greta.demoherosapi.controllers.HeroController;
import com.greta.demoherosapi.controllers.WeaponController;
import com.greta.demoherosapi.models.Hero;
import com.greta.demoherosapi.models.Weapon;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class WeaponRessourceAssembler implements RepresentationModelAssembler<Weapon, EntityModel<Weapon>> {
    @Override
    public EntityModel<Weapon> toModel(Weapon weapon) {
        return EntityModel.of(weapon,//
                linkTo(methodOn(WeaponController.class).one(weapon.getIdWeapon())).withSelfRel(),
                linkTo(methodOn(WeaponController.class).all()).withRel("weapons"));
    }

    @Override
    public CollectionModel<EntityModel<Weapon>> toCollectionModel(Iterable<? extends Weapon> entities) {
        return RepresentationModelAssembler.super.toCollectionModel(entities);
    }


}
