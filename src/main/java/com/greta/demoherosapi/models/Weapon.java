package com.greta.demoherosapi.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.server.core.Relation;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity

public class Weapon {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long idWeapon;
    private String name;
    private int attackBonus;

//    @OneToMany(mappedBy = "weapon")
//    @JsonIgnoreProperties
//    private List<Hero> hero;

    //CONSTRUCTEURS
    public Weapon() {
    }

    //GETTERS SETTERS
    public long getIdWeapon() {
        return idWeapon;
    }

    public void setIdWeapon(long idWeapon) {
        this.idWeapon = idWeapon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttackBonus() {
        return attackBonus;
    }

    public void setAttackBonus(int attackBonus) {
        this.attackBonus = attackBonus;
    }

    //EQUALS and HASHCODE

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weapon weapon = (Weapon) o;
        return idWeapon == weapon.idWeapon && attackBonus == weapon.attackBonus && name.equals(weapon.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idWeapon, name, attackBonus);
    }

    //TOSTRING

    @Override
    public String toString() {
        return "Weapon{" +
                "idWeapon=" + idWeapon +
                ", name='" + name + '\'' +
                ", attackBonus=" + attackBonus +
                '}';
    }
}
