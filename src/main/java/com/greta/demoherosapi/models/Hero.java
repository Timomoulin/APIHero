package com.greta.demoherosapi.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

@Entity
public class Hero implements Serializable {

    //Les attributs
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  long idHero;
@Size(message = "The name must be between 2 and 255 char !",min = 2,max = 255)
    private String name;
    @NotNull(message = "A hp score is mandatory")
@Min(message = "The hp score must be superior to 0",value = 1)
@Max(message = "The hp score must be inferior to 999",value = 999)
    private int hp;
    @NotNull(message = "A attack score is mandatory")
    @Min(message = "The attack score must be superior to 0",value = 1)
    @Max(message = "The attack score must be inferior or egal to 99",value = 99)

    private int attack;
    @NotNull(message = "A defense score is mandatory")
    @Min(message = "The defense score must be superior to 0",value = 1)
    @Max(message = "The defense score must be inferior or egal to 99",value = 99)

    private int defense;

//    @ManyToOne
//    @JoinColumn(name = "weapon_id")
//    @JsonIgnoreProperties
//    private Weapon weapon;

    //Constructeurs
    public Hero() {
    }

    public Hero(long idHero, String name, int hp, int attack, int defense) {
        this.idHero = idHero;
        this.name = name;
        this.hp = hp;
        this.attack = attack;
        this.defense = defense;
    }

    //Getters
    public long getIdHero() {
        return idHero;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

//    public Weapon getWeapon() {
//        return weapon;
//    }

    //Setters

    public void setIdHero(long idHero) {
        this.idHero = idHero;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

//    public void setWeapon(Weapon weapon) {
//        this.weapon = weapon;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hero hero = (Hero) o;
        return idHero == hero.idHero && hp == hero.hp && attack == hero.attack && defense == hero.defense && Objects.equals(name, hero.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idHero, name, hp, attack, defense);
    }

    @Override
    public String toString() {
        return "Hero{" +
                "idHero=" + idHero +
                ", name='" + name + '\'' +
                ", hp=" + hp +
                ", attack=" + attack +
                ", defense=" + defense +
                '}';
    }
}
