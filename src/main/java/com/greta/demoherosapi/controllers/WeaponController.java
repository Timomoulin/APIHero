package com.greta.demoherosapi.controllers;

import com.greta.demoherosapi.models.Hero;
import com.greta.demoherosapi.models.Weapon;
import com.greta.demoherosapi.services.WeaponService;
import com.greta.demoherosapi.services.assemblers.WeaponRessourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/weapons")
@CrossOrigin(value = "*")
public class WeaponController {
    @Autowired
    private WeaponService weaponService;
    @Autowired
    private WeaponRessourceAssembler weaponRessourceAssembler;

    @GetMapping
    public CollectionModel<EntityModel<Weapon>> all(){
       CollectionModel<EntityModel<Weapon>> weapons =  weaponRessourceAssembler.toCollectionModel( weaponService.getAll());
        return CollectionModel.of(weapons,linkTo(methodOn(WeaponController.class).all()).withSelfRel());
    }

    @GetMapping(value = "/{id}")
    public EntityModel<Weapon> one(@PathVariable Long id){
        return weaponRessourceAssembler.toModel(weaponService.getOne(id));
    }
}
