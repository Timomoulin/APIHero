package com.greta.demoherosapi.controllers;

import com.greta.demoherosapi.models.Hero;
import com.greta.demoherosapi.services.HeroService;
import com.greta.demoherosapi.services.assemblers.HeroRessourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
@RequestMapping("/heroes")
@CrossOrigin(value = "*")
public class HeroController {
    @Autowired
    private HeroService heroService;
    @Autowired
    private HeroRessourceAssembler heroRessourceAssembler;

    /**
     *  Recupere les heros en JSON
     *  @return les heros
     */
    @GetMapping
    public CollectionModel<EntityModel<Hero>> all() {
        List<EntityModel<Hero>> users = heroRessourceAssembler.toModel((List<Hero>) heroService.getAll());
        return CollectionModel.of(users,linkTo(methodOn(HeroController.class).all()).withSelfRel());
    }

    /**
     * Recupere un hero
     * @param id l'id du hero
     * @return le hero
     */
    @GetMapping(value = "/{id}")
    public EntityModel<Hero> one(@PathVariable  Long id){

        Hero hero= heroService.getOne(id);
        EntityModel<Hero> test=heroRessourceAssembler.toModel(hero);
        return test;
    }

    /**
     * Ajoute un nouveau hero
     * @param hero les données du hero
     * @return le hero
     */
    @PostMapping
    EntityModel<Hero> newHero(@Valid @RequestBody Hero hero) {

        return heroRessourceAssembler.toModel(heroService.saveHero(hero));
    }

    /**
     * Ajoute ou modifie un hero
     * @param newHero les nouvelles données du hero
     * @param id l'id du hero
     * @return le hero (avec les modifications)
     */
    @PutMapping("/{id}")
    EntityModel<Hero> replaceHero(@Valid @RequestBody Hero newHero, @PathVariable Long id){
        return this.heroRessourceAssembler.toModel(heroService.saveOrUpdateHero(newHero,id));
    }

    /**
     * Supprime un hero
     * @param id l'id du hero a supprimer
     */
    @DeleteMapping("/{id}")
    void deleteHero(@PathVariable Long id) {
        this.heroService.deleteHero(id);
    }
}
