package com.greta.demoherosapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoHerosApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoHerosApiApplication.class, args);
    }

}
